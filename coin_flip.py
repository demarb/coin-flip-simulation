# coin_flip.py
# Programmer: Demar Brown (Demar Codes)
# Date: Dec 24, 2020
# Program Details: This program simulates flipping a single coin however many times the user decides.
    #The code records the outcomes and counts the number of tails and heads.

    #ADDITIONAL CHALLENGE TO DO IN THE FUTURE: The program calculates the probability of heads and tails before 
    #outputting the actual results
#-----------------------------------------------

#Importing python's random library
import random

def flip_coin(num_flips):
    
    current_flip=0
    recorded_flips=[]
    num_heads= 0
    num_tails= 0

    for flip in range(0, num_flips):

        current_flip= random.randint(1,2)
        
        if current_flip == 1:
            recorded_flips.append("Head")
            num_heads+=1
        else:
            recorded_flips.append("Tail")
            num_tails+=1

    #Printing outputs 3 different ways    
    print("\nThe following flips were recorded:")
    print(recorded_flips)
    print("The number of Heads produced is: {}".format(num_heads))
    print(f"The number of Tails produced is: {num_tails}")
    
    


#Program begins here

print("\n--- COIN FLIP SIMULATION--- \n")

num_flips = int(input("How many times is the coin to be flipped? "))
flip_coin(num_flips)
